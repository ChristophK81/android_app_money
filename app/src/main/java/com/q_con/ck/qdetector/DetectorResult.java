package com.q_con.ck.qdetector;

import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cklimt on 24.07.2015.
 */
public class DetectorResult {

    private int obtainResult = -1;
    private Mat homography = null;

    private Mat logo;
    private Mat scene;

    private MatOfKeyPoint mokLogo ;
    private MatOfKeyPoint mokScene ;

    private MatOfPoint2f goodObjectPoints;
    private MatOfPoint2f goodScenePoints;

    private MatOfDMatch matches = new MatOfDMatch();
    private LinkedList<DMatch> goodMatches = new LinkedList<DMatch>();

    private Double maxDist = Double.MIN_VALUE;
    private Double minDist = Double.MAX_VALUE;
    private Double goodDist = Double.MAX_VALUE;

    public DetectorResult(){

    }

    public Mat getHomography() {
        return homography;
    }
    public void setHomography(Mat homography) {
        this.homography = homography;
    }

    public MatOfPoint2f getGoodObjectPoints() {
        return goodObjectPoints;
    }
    public void setGoodObjectPoints(MatOfPoint2f goodObjectPoints) {
        this.goodObjectPoints = goodObjectPoints;
    }

    public MatOfPoint2f getGoodScenePoints() {
        return goodScenePoints;
    }
    public void setGoodScenePoints(MatOfPoint2f goodScenePoints) {
        this.goodScenePoints = goodScenePoints;
    }

    public Mat getLogo() {
        return logo;
    }
    public void setLogo(Mat logo) {
        this.logo = logo;
    }
    public Mat getScene() {
        return scene;
    }
    public void setScene(Mat scene) {
        this.scene = scene;
    }
    public Double getMaxDist() {
        return maxDist;
    }
    public void setMaxDist(Double maxDist) {
        this.maxDist = maxDist;
    }
    public Double getMinDist() {
        return minDist;
    }
    public void setMinDist(Double minDist) {
        this.minDist = minDist;
    }

    public Double getGoodDist() {
        return goodDist;
    }
    public void setGoodDist(Double goodDist) {
        this.goodDist = goodDist;
    }

    public MatOfDMatch getMatches() {
        return matches;
    }
    public void setMatches(MatOfDMatch matches) {
        this.matches = matches;
    }

    public LinkedList<DMatch> getGoodMatches() {
        return goodMatches;
    }
    public void setGoodMatches(LinkedList<DMatch> goodMatches) {
        this.goodMatches = goodMatches;
    }

    public int getObtainResult() {
        return obtainResult;
    }
    public void setObtainResult(int obtainResult) {
        this.obtainResult = obtainResult;
    }

    public MatOfKeyPoint getMokLogo() {
        return mokLogo;
    }

    public void setMokLogo(MatOfKeyPoint mokLogo) {
        this.mokLogo = mokLogo;
    }

    public MatOfKeyPoint getMokScene() {
        return mokScene;
    }

    public void setMokScene(MatOfKeyPoint mokScene) {
        this.mokScene = mokScene;
    }
}
