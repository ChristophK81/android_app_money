package com.q_con.ck.qdetector;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import javax.xml.parsers.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.content.Context;

import static org.opencv.core.Core.split;

/**
 * Created by ck on 18.08.15.
 */


class Fragment{
    public int  xmax = 0;
    public int  xmin = 0;
    public int  ymax = 0;
    public int  ymin = 0;
    public String filename = "";
}

public class Analyser {

    Context context;
    List<Fragment> config;

    public Analyser(Context context){
        this.context = context;
        this.config = getConfig();
        context = context;
    }

    public Document getXMLDocFile() {
        Document document = null;
        File rootPath =  Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS);
        File path = new File(rootPath, "QDetetctor");
        if (!path.mkdirs()) { }
        File xmlFile = new File(path,"Extraction.xml");
        if (!xmlFile.exists())
        {
            try
            {
                xmlFile.createNewFile();
                BufferedWriter buf = new BufferedWriter(new FileWriter(xmlFile));
                buf.append("<QdetectorConfig><fragment xmin='6' ymin='16' xmax='138' ymax='106' hmin='10' hmax='20' filename='extractImage1'></fragment><fragment xmin='10' ymin='440' xmax='170' ymax='530' hmin='10' hmax='20' filename='extractImage2'></fragment><fragment xmin='880' ymin='240' xmax='970' ymax='300' hmin='10' hmax='20' filename='extractImage3'></fragment></QdetectorConfig>");
                buf.newLine();
                buf.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
        try
        {
            InputStream inputStream = new FileInputStream(xmlFile);
            document = getDocument( inputStream);
            inputStream.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }

        return document;
    }

    public Document getDocument(InputStream inputStream) {
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = factory.newDocumentBuilder();
            InputSource inputSource = new InputSource(inputStream);
            document = db.parse(inputSource);
        } catch (ParserConfigurationException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (SAXException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        } catch (IOException e) {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        return document;
    }

    public List<Fragment> getConfig() {
        ArrayList<Fragment> ret = new ArrayList<Fragment>();
        Document doc = getXMLDocFile();
        NodeList n1 = doc.getElementsByTagName("fragment");
        for (int i = 0; i < n1.getLength(); i++) {

            Element e = (Element) n1.item(i);
            Fragment fragment = new Fragment();

            try{
                fragment.xmin = Integer.parseInt(e.getAttribute("xmin"));
                fragment.xmax = Integer.parseInt(e.getAttribute("xmax"));
                fragment.ymax = Integer.parseInt(e.getAttribute("ymax"));
                fragment.ymin = Integer.parseInt(e.getAttribute("ymin"));
                fragment.filename = e.getAttribute("filename");
            }catch(NumberFormatException exc) {
                Log.e("Error: ", exc.getMessage());
            }

            ret.add(fragment);
        }

        return ret;
    }

    public boolean writeHistogram(String filename, Mat hist){

        File rootPath =  Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS);
        File path = new File(rootPath, "QDetetctor");
        if (!path.mkdirs()) { }


        File histFile = new File(path, filename );
        try
        {
            histFile.createNewFile();
        }
        catch (IOException exc) {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        try
        {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(histFile, true));
            for (int j = 0; j < hist.rows(); j++) {
                buf.append(String.format("%d - %f" , j ,  hist.get(j,0)[0]));

                buf.newLine();
            }
            buf.close();
        }
        catch (IOException exc)
        {
            // TODO Auto-generated catch block
            exc.printStackTrace();
        }

        return false;
    }
    public boolean analyse(Mat finding){
        List<Mat> ret = new ArrayList<Mat>();
        File rootPath =  Environment.getExternalStoragePublicDirectory( Environment.DIRECTORY_DOWNLOADS);
        File path = new File(rootPath, "QDetetctor");
        if (!path.mkdirs()) { }

        for (int i = 0; i < config.size(); i++) {
            Fragment fragment = config.get(i);

            Mat fragmentMat = finding.colRange(fragment.xmin, fragment.xmax).rowRange(fragment.ymin, fragment.ymax);

            List<Mat> bgrFragmentMat = new ArrayList<Mat>();
            Core.split(fragmentMat, bgrFragmentMat);

            Mat b_hist = new Mat();
            Mat g_hist = new Mat();
            Mat r_hist = new Mat();

            MatOfFloat ranges=new MatOfFloat(0,256);
            MatOfInt histSizeMat = new MatOfInt(255);
            MatOfInt channels = new MatOfInt(0);

            Mat mIntermediateMat = new Mat();
            Imgproc.cvtColor(fragmentMat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

            File file = new File(path, fragment.filename + ".png");
            Imgcodecs.imwrite(file.toString(), mIntermediateMat);

            Imgproc.calcHist(bgrFragmentMat.subList(0, 1), channels, new Mat(), b_hist, histSizeMat, ranges);
            Imgproc.calcHist(bgrFragmentMat.subList(1, 2), channels, new Mat(), g_hist, histSizeMat, ranges);
            Imgproc.calcHist(bgrFragmentMat.subList(2, 3) ,channels, new Mat(), r_hist, histSizeMat, ranges);

            writeHistogram(fragment.filename + "_HISTOGRAM_B.txt", b_hist);
            writeHistogram(fragment.filename + "_HISTOGRAM_G.txt", g_hist);
            writeHistogram(fragment.filename + "_HISTOGRAM_R.txt", r_hist);
        }

        return true;
    }
}
