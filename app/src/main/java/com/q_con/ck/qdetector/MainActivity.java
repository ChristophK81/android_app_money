package com.q_con.ck.qdetector;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends Activity implements CvCameraViewListener2 { //extends ActionBarActivity

    private static final String TAG = "LogDetector";

    private CameraBridgeViewBase mOpenCvCameraView;
    private boolean              mShowWebsite  = true;
    private MenuItem             mItemSwitchShowWebsite = null;
    private MenuItem             mItemSwitchPrefs = null;

    private Detector             detector;
    private Mat                  mRgba;
    private Mat                  mResultRgba;
    private int                  takePicture;


    static{ System.loadLibrary("opencv_java3"); }
    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        Log.d(TAG, "called onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.java_surface_view);

        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setCameraIndex(mOpenCvCameraView.CAMERA_ID_BACK) ;

        Button btnScan = (Button)findViewById(R.id.scan_btn);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture += 1;
            }
        });

        detector = new Detector(getApplicationContext());
        detector.setLogoInformation(loadLogos());
    }

    protected List<Mat> loadLogos() {

        List<Mat> lstImg = new ArrayList<Mat> ();
        try{
            /*
            lstImg.add( Utils.loadResource(getApplicationContext(), R.drawable.org100_weich_png_small ,1));
            lstImg.add( Utils.loadResource(getApplicationContext(), R.drawable.org100_scharf_png_small ,1));
            */
            lstImg.add( Utils.loadResource(getApplicationContext(), R.drawable.org100_weich_png ,1));
            lstImg.add( Utils.loadResource(getApplicationContext(), R.drawable.org100_scharf_png ,1));

        }catch(Exception e){
            Log.e( TAG, "Mistake in loadLogos " + e.toString());
        }

        return lstImg;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_0_0, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        Log.i(TAG, "called onCreateOptionsMenu");
         mItemSwitchShowWebsite = menu.add("Open Website/only show finding");
        //mItemSwitchPrefs = menu.add("Settings");
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        String toastMesage = new String();
        Log.i(TAG, "called onOptionsItemSelected; selected item: " + item);
        /*
        if (item == mItemSwitchCamera) {
            mOpenCvCameraView.setVisibility(SurfaceView.GONE);

            mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.java_surface_view);
            toastMesage = "Java Camera";


            mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE);
            mOpenCvCameraView.setCvCameraViewListener(this);
            mOpenCvCameraView.enableView();
            Toast toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG);
            toast.show();
        }
        else */
        if (item == mItemSwitchShowWebsite) {
            mShowWebsite = !mShowWebsite;
            if(mShowWebsite){
                toastMesage = "Show website";
            }else{
                toastMesage = "Only show hit";
            }
            Toast toast = Toast.makeText(this, toastMesage, Toast.LENGTH_LONG);
            toast.show();
        }else if (item == mItemSwitchPrefs){

            Intent i = new Intent(this, SettingsActivity.class);
            startActivityForResult( i, 1);

        }

        return true;

    }

    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
    }

    public void onCameraViewStopped() {
    }

    public Mat onCameraFrame(final CvCameraViewFrame inputFrame) {

        mRgba = inputFrame.rgba();

        /*
        try{
               mRgba =  Utils.loadResource(getApplicationContext(), R.drawable.original_scene ,1);
        } catch (Exception e) {
            Log.e(TAG, String.format("Error LoadScene  %s ", e.getMessage()));
        }
        */

        //Mat resizeimage = new Mat();
        //Size sz = new Size(mRgba.cols()/2,mRgba.rows()/2);
        //Imgproc.resize( mRgba, resizeimage, sz );

        if(takePicture == 1) {
            takePicture += 1;

            mResultRgba = detector.detect(mRgba, "");


            try {
                Log.e(TAG, String.format("*********************start ************************"));
                /*
                int res = 0;
                switch(res) {
                    case 0: {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "Q-fin detected", Toast.LENGTH_LONG);
                                toast.show();

                            }
                        });

                        break;
                    }
                    case 1: {

                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "glashutte logo detected", Toast.LENGTH_LONG);
                                toast.show();
                                if (mShowWebsite) {
                                     Intent i = new Intent(Intent.ACTION_VIEW,
                                            Uri.parse("http://www.glashuette-original.com/start/"));

                                    startActivity(i);

                                    final Bitmap original = BitmapFactory.decodeResource(getResources(), R.drawable.original);
                                    Bitmap bitmap = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);

                                    Utils.matToBitmap(mRgba, bitmap);

                                            boolean isOriginal = Comparator.compare(original, bitmap);
                                            Log.d("Main", "The pictures are origianl: " + isOriginal);



                                    if (isOriginal){
                                        Toast toast1 = Toast.makeText(MainActivity.this, "this Img is Original ", Toast.LENGTH_LONG);
                                        toast.show();}

                                    else  {
                                            Toast toast2 = Toast.makeText(MainActivity.this, "this Img is not Original ", Toast.LENGTH_LONG);
                                            toast.show();

                                }
                            }
                        });

                        break;
                    }
                    default: {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast toast = Toast.makeText(MainActivity.this, "Nothing found", Toast.LENGTH_LONG);
                                toast.show();
                            }
                    });
                }

                }
                */
            } catch (Exception e) {

                Log.e(TAG, String.format("*******!!!!!!!!!!!!!!******** %s ************************", e.getMessage()));
            }

        } else if (takePicture == 2) {
            Imgproc.putText(mResultRgba,  "Display Results" , new Point(50, 50), 3, 1, new Scalar(255, 0, 0, 255), 2);
            return  mResultRgba;

        } else if (takePicture > 2) {
            takePicture = 0;
        }

        /*
        Mat mRgbaT = mRgba.t();
        Core.flip(mRgba.t(), mRgbaT,1);
        Imgproc.resize(mRgbaT, mRgbaT,mRgba.size());
        */

        Mat cleanOut = inputFrame.rgba();
        Imgproc.putText(cleanOut, "Ready", new Point(50, 50), 3, 1, new Scalar(255, 0, 0, 255), 2);

        return  cleanOut;
    }

}
