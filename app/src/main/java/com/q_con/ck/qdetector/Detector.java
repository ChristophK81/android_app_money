package com.q_con.ck.qdetector;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
//import org.opencv.features2d.DMatch;
import org.opencv.core.DMatch;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.features2d.Features2d;
import org.opencv.core.KeyPoint;
//import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.q_con.ck.qdetector.DetectorResult;
import com.q_con.ck.qdetector.Comparator;

public class Detector {

    private static final String TAG = "LogDetector";


    private FeatureDetector mDetector ;
	private DescriptorExtractor mExtractor;
	private DescriptorMatcher mMatcher;
	
	
	private List<Mat> mLogos;
	private Mat mFrame;
	
	private List<MatOfKeyPoint> mokLogos;
	private MatOfKeyPoint mokFrame;
	
	private List<Mat> descriptorLogos;
	private Mat descriptorFrame;
	
	double DISTANCE_FAKTOR = 2.5;
	
	boolean DEBUG = true;
    boolean DEBUG_FILE = true;
	String DEBUG_PATH = "/";
	Analyser analyser;

	public Detector( Context context){

		this.mDetector = FeatureDetector.create(FeatureDetector.ORB);
		this.mExtractor = DescriptorExtractor.create(DescriptorExtractor.ORB);
		this.mMatcher= DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMINGLUT);

		analyser = new Analyser(context);

	}

    void logMessage(int msg){
        logMessage( String.format("%d", msg));
    }

    void logMessage(double msg){
        logMessage(String.format("%f", msg));
    }

    void logMessage(String msg){
        Log.d(TAG, msg);
		if (DEBUG_FILE)
		{
			File path = getAlbumStorageDir("QDetetctor");
			File logFile = new File(path,"log.file");
			if (!logFile.exists())
			{
				try
				{
					logFile.createNewFile();
				}
				catch (IOException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try
			{
				//BufferedWriter for performance, true to set append to file flag
				BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
				buf.append(msg);
				buf.newLine();
				buf.close();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}

	void setLogoInformation(List<Mat> lLogos){

		this.mLogos = lLogos;
		this.descriptorLogos =  new ArrayList<Mat> ();
		this.mokLogos = new ArrayList<MatOfKeyPoint>();
		
		for(int i=0; i < this.mLogos.size(); i++){
			
			this.mokLogos.add(new MatOfKeyPoint());
			this.mDetector.detect(this.mLogos.get(i), this.mokLogos.get(i));
			this.descriptorLogos.add(new Mat());
			this.mExtractor.compute(this.mLogos.get(i), this.mokLogos.get(i), this.descriptorLogos.get(i));

            if (DEBUG) {

                logMessage(String.format("DescriptorLogo %d count %s", i, this.descriptorLogos.get(i).size()));
                logMessage(String.format("KeyPointLogo %d count %s", i,this.mokLogos.get(i).size()));
            }

		}
		
    	long endTime = System.nanoTime();
    	

        if (DEBUG_FILE) {
    		for(int i=0; i < this.mLogos.size(); i++){
    			Mat outputLogoImg = new Mat();
    			Features2d.drawKeypoints( this.mLogos.get(i), this.mokLogos.get(i), outputLogoImg);
                SaveImage(outputLogoImg,String.format("name_%d.png",i));
    		}
    	}
	}

    public File getAlbumStorageDir(String albumName) {
        // Get the directory for the user's public pictures directory.
        File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS), albumName);
        if (!file.mkdirs()) {
			//logMessage( "Directory not created");
        }
        return file;
    }


    public void SaveImage (Mat mat,String filename) {
        Mat mIntermediateMat = new Mat();

        Imgproc.cvtColor(mat, mIntermediateMat, Imgproc.COLOR_RGBA2BGR, 3);

        File path = getAlbumStorageDir("QDetetctor");

        File file = new File(path, filename);
		logMessage(file.toString());
        Boolean bool = null;
        filename = file.toString();
        bool = Imgcodecs.imwrite(filename, mIntermediateMat);

        if (bool == true)
			logMessage("SUCCESS writing image to external storage");
        else
			logMessage( "Fail writing image to external storage");
    }

	void setSceneInformation(Mat mScene){
        this.mFrame = new Mat();

		long startTime = System.nanoTime();
        // CKL upgrade to 3.0Imgproc.cvtColor(mScene, this.mFrame, Imgproc.COLOR_RGBA2RGB, new Scalar(4));
        Imgproc.cvtColor(mScene, this.mFrame, Imgproc.COLOR_RGBA2RGB);
        long endTime = System.nanoTime();

        if (DEBUG) {
            logMessage(String.format("convert rgba to rgb %d", (endTime - startTime) / 1000000));
        }


		this.mokFrame = new MatOfKeyPoint();
		this.descriptorFrame = new Mat();
		mDetector.detect(this.mFrame, this.mokFrame);
		mExtractor.compute(this.mFrame, this.mokFrame, this.descriptorFrame);
				        
		endTime = System.nanoTime();
			    	
	    if (DEBUG) {
            logMessage(String.format("Compute Scene in ms: %d", (endTime - startTime) / 1000000));
            logMessage(String.format("DescriptorScene count %s", this.descriptorFrame.size().toString() ));
            logMessage(String.format("KeyPointScene count %s", this.mokFrame.size().toString()));

        }
        
        if (DEBUG_FILE){
			Mat outputKeypointImg = new Mat();
            Features2d.drawKeypoints( mFrame, this.mokFrame , outputKeypointImg);
            SaveImage(mScene,"original_scene.png");
            SaveImage(outputKeypointImg,"all_scene_keypoints.png");
	    }
	}
	
	int checkHomography(Mat  H)
    {

	  double det = H.get(0, 0)[0] * H.get( 1, 1)[0] - H.get( 1, 0)[0] * H.get( 0, 1)[0];
	  if (det < 0)
	  {	
		  if (DEBUG){
			  logMessage(det);
		  }
		  return 1;
	  }
	  
	  double N1 = Math.sqrt(H.get(0, 0)[0] * H.get(0, 0)[0] + H.get( 1, 0)[0] * H.get( 1, 0)[0]);
	  if (N1 > 4 || N1 < 0.1)
	  {
		  if (DEBUG){
			  logMessage(N1);
		  }
		  return 2;
	  }
	  
	  double N2 = Math.sqrt( H.get(  0, 1)[0] *  H.get(  0, 1)[0] + H.get( 1, 1)[0] * H.get( 1, 1)[0]);
	  if (N2 > 4 || N2 < 0.1)
	  {
		  if (DEBUG){
			  logMessage(N2);
		  }
		  return 3;
	  }
	  
	  double N3 = Math.sqrt( H.get(  2, 0)[0] *  H.get( 2, 0)[0] +  H.get(  2, 1)[0] *  H.get(  2, 1)[0]);
	  if (N3 > 0.005) //original 0.02
	  {
		  if (DEBUG){
			  logMessage(N3);
		  }
		  return 4;
	  }

	  return 0;

	}


	public Mat detect(Mat mScene, String errorMsg){
        Mat mOrginal = mScene.clone();

		this.setSceneInformation(mScene);
		long startTime = System.nanoTime();
		List<DetectorResult> lDetectorResults = new ArrayList<DetectorResult>();

		for(int i=0;i<mokLogos.size();i++) {

			DetectorResult detectorResult = new DetectorResult();
			lDetectorResults.add(detectorResult);

			MatOfKeyPoint mokLogo = this.mokLogos.get(i);
			detectorResult.setMokLogo(mokLogo);

			mMatcher.match(descriptorLogos.get(i), descriptorFrame, detectorResult.getMatches());

			List<DMatch> matchesList = detectorResult.getMatches().toList();
			LinkedList<DMatch> goodMatches = new LinkedList<DMatch>();

			if (matchesList.size() < 4) {
				detectorResult.setGoodMatches(goodMatches);
				continue;
			}

			Double max_dist = 0.0;
			Double min_dist = Double.MAX_VALUE;

			for (int j = 0; j < matchesList.size(); j++) {
				Double dist = (double) matchesList.get(j).distance;
				if (dist < min_dist)
					min_dist = dist;
				if (dist > max_dist)
					max_dist = dist;
			}

			detectorResult.setMinDist(min_dist);
			detectorResult.setMaxDist(max_dist);
			detectorResult.setGoodDist(DISTANCE_FAKTOR * min_dist);

			for (int j = 0; j < matchesList.size(); j++) {
				//if (matchesList.get(j).distance < (DISTANCE_FAKTOR * min_dist))
				if (matchesList.get(j).distance < 50)
					goodMatches.addLast(matchesList.get(j));
			}

			if (DEBUG) {
				//logMessage(String.format("logo %d: all/good matches %d/%d - max %f min %f DISTANCE_FAKTOR %f lim %f ", i, matchesList.size(), goodMatches.size(), max_dist, min_dist ,DISTANCE_FAKTOR, DISTANCE_FAKTOR * min_dist));
				logMessage(String.format("logo %d: all/good matches %d/%d - max %f min %f DISTANCE_FAKTOR %f lim %f ", i, matchesList.size(), goodMatches.size(), max_dist, min_dist ,DISTANCE_FAKTOR, DISTANCE_FAKTOR * min_dist));
			}

			if (goodMatches.size() < 4) {
				goodMatches.clear();
			}

			lDetectorResults.get(i).setGoodMatches(goodMatches);

			if (DEBUG_FILE) {

				Mat outputImg = new Mat();
				Mat newScene = new Mat();
				MatOfByte drawnMatches = new MatOfByte();

				//Imgproc.cvtColor(mFrame, newScene, Imgproc.COLOR_RGBA2RGB);
				if (goodMatches.size() < 4) {
					if (DEBUG) {
						logMessage(String.format("No 4 matches for %d", i));
					}
					continue;
				}
				MatOfDMatch tmpGoodMatches = new MatOfDMatch();
				tmpGoodMatches.fromList(goodMatches);

				Features2d.drawMatches(mLogos.get(i), mokLogos.get(i), mFrame, mokFrame, tmpGoodMatches, outputImg, new Scalar(0, 255, 0), new Scalar(0, 0, 255), drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);
				Imgcodecs.imwrite(String.format("%s/matches_%d_scene.png", DEBUG_PATH, i), outputImg);
			}

			List<KeyPoint> keypoints_sceneList = this.mokFrame.toList();
			List<KeyPoint> keypoints_objectList = this.mokLogos.get(i).toList();

			LinkedList<Point> objList = new LinkedList<Point>();
			LinkedList<Point> sceneList = new LinkedList<Point>();

			for (int j = 0; j < goodMatches.size(); j++) {
				objList.addLast(keypoints_objectList.get(goodMatches.get(j).queryIdx).pt);
				sceneList.addLast(keypoints_sceneList.get(goodMatches.get(j).trainIdx).pt);
			}

			MatOfPoint2f obj = new MatOfPoint2f();
			MatOfPoint2f scene = new MatOfPoint2f();

			obj.fromList(objList);
			scene.fromList(sceneList);

			if (DEBUG_FILE) {
				Mat outputImg = new Mat();
				MatOfByte drawnMatches = new MatOfByte();
				MatOfDMatch good_Matches = new MatOfDMatch();
				good_Matches.fromList(goodMatches);

				Features2d.drawMatches(mLogos.get(i), mokLogos.get(i), mFrame, mokFrame, good_Matches, outputImg, new Scalar(0, 255, 0), new Scalar(0, 0, 255), drawnMatches, Features2d.NOT_DRAW_SINGLE_POINTS);

				SaveImage(outputImg, String.format("matches_scene_%d.png", i));
			}

			if (!(scene.size().height > 0 && obj.size().height > 0 && scene.size().height == obj.size().height)) {
				logMessage(String.format("findHomo %s - %s", obj.size().toString(), scene.size().toString()));
				continue;
			}
			Mat H;
			try {
				H = Calib3d.findHomography(obj, scene, Calib3d.RANSAC, 5);
			} catch (Exception e) {
				errorMsg = "error by findHomography\n" + e.getMessage();
				continue;
			}
			detectorResult.setHomography(H);
            detectorResult.setObtainResult(checkHomography(H));

			if (DEBUG) {
				long endTime = System.nanoTime();
				logMessage(String.format("checkHomography %d - Compute in ms: %d", checkHomography(H), (endTime - startTime) / 1000000));
			}

            if (DEBUG_FILE) {
                Mat tmp_corners = new Mat(4, 1, CvType.CV_32FC2);
                Mat scene_corners = new Mat(4, 1, CvType.CV_32FC2);

                Mat mLogo = mLogos.get(i);
                //get corners from object
                tmp_corners.put(0, 0, new double[]{0, 0});
                tmp_corners.put(1, 0, new double[]{mLogo.cols(), 0});
                tmp_corners.put(2, 0, new double[]{mLogo.cols(), mLogo.rows()});
                tmp_corners.put(3, 0, new double[]{0, mLogo.rows()});

                try {
                    Core.perspectiveTransform(tmp_corners, scene_corners, H);
                } catch (Exception e) {
                    logMessage("error by perspectiveTransform:" + e.getMessage());
                }

                Imgproc.line(mFrame, new Point(scene_corners.get(0, 0)), new Point(scene_corners.get(1, 0)), new Scalar(0, 255, 0), 4);
                Imgproc.line(mFrame, new Point(scene_corners.get(1, 0)), new Point(scene_corners.get(2, 0)), new Scalar(0, 255, 0), 4);
                Imgproc.line(mFrame, new Point(scene_corners.get(2, 0)), new Point(scene_corners.get(3, 0)), new Scalar(0, 255, 0), 4);
                Imgproc.line(mFrame, new Point(scene_corners.get(3, 0)), new Point(scene_corners.get(0, 0)), new Scalar(0, 255, 0), 4);

                SaveImage(mFrame, String.format("matches_scene_rec_%d.png", i));
            }
		}

        double bestFaktor = -1;
        int bestMatchesCount = -1;
        int bestChoice = -1;

        /*
        for(int i=0;i<lDetectorResults.size();i++) {
            DetectorResult detectorResult =lDetectorResults.get(i);
            int goodMatchesCount =  detectorResult.getGoodMatches().size();
            double minDist = detectorResult.getMinDist();

            if (goodMatchesCount > 0) {
                double factor = goodMatchesCount / minDist;
                if (bestFaktor < factor){
                    bestChoice = i;
                    bestFaktor = factor;
                }
            }
        }
        */

        for(int i=0;i<lDetectorResults.size();i++) {
            DetectorResult detectorResult =lDetectorResults.get(i);
            int goodMatchesCount =  detectorResult.getGoodMatches().size();

            if (detectorResult.getObtainResult() == 0 && goodMatchesCount > bestMatchesCount) {
                bestMatchesCount = goodMatchesCount ;
                bestChoice = i;
            }
        }

        Mat quad;

        if (bestChoice > -1) {
            Mat tmp_corners = new Mat(4, 1, CvType.CV_32FC2);
            Mat scene_corners = new Mat(4, 1, CvType.CV_32FC2);

            Mat mLogo = mLogos.get(bestChoice);
            //get corners from object
            tmp_corners.put(0, 0, new double[]{0, 0});
            tmp_corners.put(1, 0, new double[]{mLogo.cols(), 0});
            tmp_corners.put(2, 0, new double[]{mLogo.cols(), mLogo.rows()});
            tmp_corners.put(3, 0, new double[]{0, mLogo.rows()});

            Mat H = lDetectorResults.get(bestChoice).getHomography();

            try {
                Core.perspectiveTransform(tmp_corners, scene_corners, H);
            } catch (Exception e) {
                logMessage("error by perspectiveTransform:" + e.getMessage());
            }

            Imgproc.line(mOrginal, new Point(scene_corners.get(0, 0)), new Point(scene_corners.get(1, 0)), new Scalar(0, 255, 0), 4);
            Imgproc.line(mOrginal, new Point(scene_corners.get(1, 0)), new Point(scene_corners.get(2, 0)), new Scalar(0, 255, 0), 4);
            Imgproc.line(mOrginal, new Point(scene_corners.get(2, 0)), new Point(scene_corners.get(3, 0)), new Scalar(0, 255, 0), 4);
            Imgproc.line(mOrginal, new Point(scene_corners.get(3, 0)), new Point(scene_corners.get(0, 0)), new Scalar(0, 255, 0), 4);

            quad = new Mat(mLogo.size(), CvType.CV_32F);
            Imgproc.warpPerspective(mOrginal, quad, H, quad.size(), Imgproc.CV_WARP_INVERSE_MAP);

            Mat upperLeft = new Mat();
            Mat bottomLeft = new Mat();
            Mat middleRight  = new Mat();

			Mat upperLeftOrg =  new Mat();
			Mat bottomLeftOrg = new Mat();
			Mat middleRightOrg =  new Mat();


			analyser.analyse(quad);
			/*
			if ( bestChoice > 1) {
                upperLeft = quad.colRange(6, 138).rowRange(16, 106);
                bottomLeft = quad.colRange(10, 170).rowRange(440, 530);
                middleRight = quad.colRange(880, 970).rowRange(240, 300);

				int minX = 71;
				int minY = 10 ;
				int maxX = 161;
				int maxY = 142;
				upperLeft.copyTo(mOrginal.colRange(minY, maxY).rowRange(minX, maxX));
				Imgproc.rectangle(mOrginal, new Point(minY - 1, minX - 1), new Point(maxY + 1, maxX + 1), new Scalar(255, 0, 0), 4);

				minX = 304;
				minY = 10 ;
				maxX = 394;
				maxY = 170;
				bottomLeft.copyTo(mOrginal.colRange(minY, maxY).rowRange(minX, maxX));
				Imgproc.rectangle(mOrginal, new Point(minY - 1, minX - 1), new Point(maxY + 1, maxX + 1), new Scalar(255, 0, 0), 4);

				minX = 200;
				minY = 10 ;
				maxX = 260;
				maxY = 100;
				middleRight.copyTo(mOrginal.colRange(minY, maxY).rowRange(minX, maxX));
				Imgproc.rectangle(mOrginal, new Point(minY - 1, minX - 1), new Point(maxY + 1, maxX + 1), new Scalar(255, 0, 0), 4);

				upperLeftOrg = mLogos.get(2).colRange(6, 138).rowRange(16, 106);
				bottomLeftOrg = mLogos.get(2).colRange(10, 170).rowRange(440, 530);
				middleRightOrg = mLogos.get(2).colRange(880, 960).rowRange(240, 300);

			} else {
                upperLeft = quad.colRange(3, 69).rowRange(8, 53);
                bottomLeft = quad.colRange(5, 85).rowRange(220, 265);
                middleRight = quad.colRange(440, 485).rowRange(120, 150);

				upperLeft.copyTo(mOrginal.colRange(10, 76).rowRange(66, 111));
				bottomLeft.copyTo(mOrginal.colRange(10, 90).rowRange(304, 349));
				middleRight.copyTo(mOrginal.colRange(10, 45).rowRange(200, 230));

				upperLeftOrg = mLogos.get(0).colRange(3, 69).rowRange(8, 53);
				bottomLeftOrg = mLogos.get(0).colRange(5, 85).rowRange(220, 265);
				middleRightOrg = mLogos.get(0).colRange(440, 480).rowRange(60, 150);
			}
			*/
            /*
            boolean bUpperLeft = Comparator.comparePeaks( upperLeftOrg, upperLeft) ;
            boolean bBottomLeft = Comparator.comparePeaks( bottomLeftOrg, bottomLeft) ;
            boolean bMiddleRight = Comparator.comparePeaks( middleRightOrg, middleRight) ;

            if (DEBUG) {
                logMessage(String.format("UpperLeft %b, bBottomLeft %b,bMiddleRight %b", bUpperLeft,bBottomLeft,bMiddleRight));
            }
            */

            if (DEBUG_FILE) {

                SaveImage(quad, "cutted.png");

            }
        }

        if (DEBUG_FILE) {

            SaveImage(mOrginal, "matches_result.png");
        }

        return mOrginal;
	}
	
}


