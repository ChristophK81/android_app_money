package com.q_con.ck.qdetector;

import java.util.ArrayList;

/**
 * Created by albertisaac on 24/07/15.
 */
public class Peaks {
    public ArrayList<Integer> getIndicies() {
        return indicies;
    }

    public void setIndicies(ArrayList<Integer> indicies) {
        this.indicies = indicies;
    }

    public ArrayList<Integer> getValues() {
        return values;
    }

    public void setValues(ArrayList<Integer> values) {
        this.values = values;
    }

    ArrayList<Integer> indicies = new ArrayList<Integer>();
    ArrayList<Integer> values = new ArrayList<Integer>();

    public Peaks(ArrayList<Integer> i, ArrayList<Integer> v){
        indicies = i;
        values = v;

    }
}
