package com.q_con.ck.qdetector;


import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.MatOfFloat;
import org.opencv.core.MatOfInt;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.opencv.core.Core.split;

/**
 * Created by ElieB on 7/16/2015.
 */
public class GetHistoOpenCV {

    public static final String TAG = GetHistoOpenCV.class.getSimpleName();


    public static Mat toGrayscale(Mat img) {

        Mat grayImg = new Mat();

        Imgproc.cvtColor(img, grayImg, Imgproc.COLOR_RGB2GRAY, 0);

        return grayImg;
    }

    public static Peaks findAllMax(Mat histogram){
        int max = -1;
        ArrayList<Integer> maxValues = new ArrayList<Integer>();
        ArrayList<Integer> maxIndices = new ArrayList<Integer>();
        Peaks peaks = new Peaks(maxIndices, maxValues);

        for (int y = 0; y < histogram.rows(); y++){
            Log.d("y is ", " " + y + "  " + (int) histogram.get(y, 0)[0]);
            int value = (int)histogram.get(y,0) [0];
            if ((int)histogram.get(y,0) [0] > (int)histogram.get(y-1,0) [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y + 1,0)  [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y-2,0) [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y+2,0) [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y-3,0) [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y+3,0) [0] ){
            //if ((int)histogram.get(y,0) [0] > (int)histogram.get(y-1,0) [0] && (int)histogram.get(y,0) [0] > (int)histogram.get(y + 1,0)  [0]){
            max = (int)histogram.get(y,0) [0];
                maxValues.add(max);
                maxIndices.add(y);
            }




        }
        return peaks;
    }


    public static Mat makeHist(Mat img) {

        List<Mat> matList = new LinkedList<Mat>();
        matList.add(img);

        //separate the colors
        List<Mat> bgr_planes = null;
        split(img, bgr_planes);

        bgr_planes.get(0);



        Mat b_hist = new Mat();
        Mat g_hist = new Mat();
        Mat r_hist = new Mat();



        //int histSize = 255;
       // Mat histogram = new Mat();
        MatOfFloat ranges=new MatOfFloat(0,256);
        MatOfInt histSizeMat = new MatOfInt(255);
        //Imgproc.calcHist(matList, new MatOfInt(0), new Mat(), histogram, histSizeMat, ranges);
        //Log.d("printing the histo", "  " + (int) histogram.get(1, 0)[0]);
        Imgproc.calcHist((List<Mat>) bgr_planes.get(0), new MatOfInt(0), new Mat(), b_hist, histSizeMat, ranges);
        Imgproc.calcHist((List<Mat>) bgr_planes.get(1), new MatOfInt(0), new Mat(), g_hist, histSizeMat, ranges);
        Imgproc.calcHist((List<Mat>) bgr_planes.get(2), new MatOfInt(0), new Mat(), r_hist, histSizeMat, ranges);




// Create space for histogram image
      //  Mat histImage = Mat.zeros(100, (int) histSize.get(0, 0)[0], CvType.CV_8UC1);
// Normalize histogram
      //  Core.normalize(histogram, histogram, 1, histImage.rows(), Core.NORM_MINMAX, -1, new Mat());

// transform the image into Bitmap
       // Bitmap bitmap = Bitmap.createBitmap(histogram.cols(), histogram.rows(), Bitmap.Config.ARGB_8888);
       // Utils.matToBitmap(histogram, bitmap);

        return b_hist;

    }
}



