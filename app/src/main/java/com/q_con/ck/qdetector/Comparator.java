package com.q_con.ck.qdetector;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.core.Mat;

import java.util.ArrayList;

/**
 * Created by albertisaac on 11/07/15.
 */
public class Comparator {


    public static boolean comparePeaks(Mat template, Mat real) {
        Peaks cameraPeaks       = GetHistoOpenCV.findAllMax(real);
        Peaks testQfinPeaks     = GetHistoOpenCV.findAllMax(template);

        ArrayList<Integer> cameraIndices     = cameraPeaks.getIndicies();
        ArrayList<Integer> templateIndices   = testQfinPeaks.getIndicies();
        ArrayList<Integer> cameraValues     = cameraPeaks.getValues();
        ArrayList<Integer> templateValues   = testQfinPeaks.getValues();
        Log.d("the idxs are ", " " + cameraIndices);
        Log.d("the VALS are ", " " + cameraValues);
        Log.d("the idxs are ", " " + templateIndices);
        Log.d("the VALS are ", " " + templateValues);

        for(int i=0; i<cameraIndices.size(); i++) {
            int cidx = cameraIndices.get(i);
            int cval = cameraValues.get(i);
            int tidx = templateIndices.get(i);
            int tval = templateValues.get(i);

            if(cidx == tidx || tidx == cidx + 1 || tidx == cidx + 2 || tidx == cidx + 3 || tidx == cidx - 1 || tidx == cidx - 2 || tidx == cidx - 3) {

                int d = Math.abs(cval - tval);
                if(d > 200) {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }


    public static boolean compare(Bitmap original, Bitmap image) {
        int height = 640;
        int width = 480;

        for(int y=0; y<height; y++) {
            Log.d("adsf", y + "");
            for(int x=0; x<width; x++) {
                if(Math.abs(original.getPixel(x, y) - image.getPixel(x, y)) < 1) {
                    return false;
                }
            }
        }
        return true;
    }
}
